package sample;

import javafx.scene.control.Button;

public class preUser implements java.io.Serializable{
    public String id;
    public String name;
    public String surname;
    public String position;
    public String login = " ";
    public String password = " ";

    preUser(String _id, String _name, String _surname, String _position){
        id = _id;
        name = _name;
        surname = _surname;
        position = _position;

    }
    public String getId(){
        return id;
    }
    public String getName(){
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPosition() {
        return position;
    }


}
