package sample;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.control.Button;

import java.io.*;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

class ClientThread implements Runnable{
    private Socket clientSocket;
    private int id;
    private String serverInfo = "Input '\\?', Add arg1 [argn]', 'Show' or 'Quit'.";

    ObjectInputStream is;
    ObjectOutputStream os;

    ClientThread(Socket socket, int id,ObjectOutputStream _os, ObjectInputStream _is)
    {
        this.clientSocket = socket;
        is=_is;
        os=_os;
    }
    public void run() {

        new JFXPanel(); // this will prepare JavaFX toolkit and environment
        Platform.runLater(new Runnable() {
            public void run() {
                boolean flag = true;
                while (clientSocket.isConnected()) {
                    Order order = null;

                    try {
                        order = (Order) is.readObject();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    try {
                        switch (order.command) {
                            case "1":
                                DB_Controller.Add(order.name, order.surname, order.position, order.login, order.password);
                                os.writeObject("1");
                                break;
                            case "2":

                                DB_Controller.show();
                                try {
                                    boolean foundedLogin = false;
                                    boolean foundedPassword = false;
                                    while (DB_Controller.resultSet.next()) {

                                        String login = DB_Controller.resultSet.getString(5);
                                        String password = DB_Controller.resultSet.getString(6);

                                        if (login.equals(order.login)) foundedLogin = true;
                                        if (password.equals(order.password)) foundedPassword = true;

                                    }
                                    if (foundedLogin == true && foundedPassword == true) {
                                        os.writeObject("1");
                                    }
                                    if (foundedLogin == false && foundedPassword == false) os.writeObject("2");

                                } catch (java.sql.SQLException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case "getUsersTable":
                                System.out.print("case 1");
                                DB_Controller.getUsersTable();
                                List<TableUsers> tableUsers = new ArrayList<TableUsers>();
                                while (DB_Controller.resultSet.next()) {
                                    TableUsers tableUsers1 = new TableUsers(Integer.toString((DB_Controller.resultSet.getInt(1))), DB_Controller.resultSet.getString(2),
                                            DB_Controller.resultSet.getString(3), DB_Controller.resultSet.getString(4),
                                            DB_Controller.resultSet.getString(5), DB_Controller.resultSet.getString(6));

                                    tableUsers.add(tableUsers1);
                                }
                                os.writeObject(tableUsers);
                                break;
                            case "getPreUsersTable":
                                System.out.print("case 2");
                                DB_Controller.getPreUsersTable();
                                List<preUser> preUsers = new ArrayList<preUser>();
                                while (DB_Controller.resultSet.next()) {
                                    preUser tablepre = new preUser(Integer.toString((DB_Controller.resultSet.getInt(1))), DB_Controller.resultSet.getString(2),
                                            DB_Controller.resultSet.getString(3), DB_Controller.resultSet.getString(4));

                                    preUsers.add(tablepre);
                                }
                                os.writeObject(preUsers);
                                break;
                            case "edit user":
                                System.out.print("\n1) ");
                                DB_Controller.EditUsers(order.login, order.password, order.name, order.surname, order.position);
                                os.writeObject("1");
                                break;
                            case "edit user properties":
                                System.out.print("\n2) ");
                                DB_Controller.EditUserProperties(order.login, order.password, order.name);
                                os.writeObject("1");
                                break;

                            case "add user":
                                DB_Controller.AddUser(order.name, order.surname, order.position, order.login, order.password);
                                DB_Controller.Search(order.name, order.surname);
                                String answer = " ";
                                while (DB_Controller.resultSet.next()) {
                                     answer= Integer.toString(DB_Controller.resultSet.getInt(1));
                                }
                                System.out.print(" answer: "+ answer);
                                os.writeObject(answer);
                                break;
                            case "add user properties":
                                DB_Controller.EditUserProperties(order.login, order.password, order.name);
                                os.writeObject("1");
                                break;
                            case "delete user":
                                DB_Controller.DeleteUser(order.login);
                                os.writeObject("0");
                                break;
                            case "find user":
                                DB_Controller.FindUser(order.name, order.surname, order.position, order.login, order.password);
                                //System.out.print("Founded ID:"+DB_Controller.resultSet.getString(0));
                                os.writeObject(DB_Controller.resultSet.getString(0));
                                break;
                            case "get preuser":
                                break;
                            case "get user":
                                DB_Controller.getUser(order.login);
                                break;
                            case "fromPreUserToUser":
                                DB_Controller.fromPreUserToUser(order.login);
                                System.out.print("\npreuser ID:"+order.login);
                                os.writeObject("0");
                                break;
                            case "delete preuser":
                                DB_Controller.DeletePreUser(order.login);
                                os.writeObject("0");
                                break;
                            default:
                                //writer.write("Invalid syntax. " + serverInfo);
                                break;
                        }

                    } catch (IOException | SQLException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    //writer.close();
                    //reader.close();
                    clientSocket.close();
                    System.out.println("Client #" + id + " successfully disconnected.");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }}


