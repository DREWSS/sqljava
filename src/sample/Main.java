package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle(" ");
        primaryStage.setScene(new Scene(root, 645, 740));
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();
        new Controller().GetInfo(root,primaryStage);

    }


    public static void main(String[] args) {

        new JFXPanel();
        Platform.runLater(new Runnable() {
            @Override
            public void run() {

            }
        });
        launch(args);

    }
}
