package sample;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class clientConnection {

    private static clientConnection connection;
    private static ObjectInputStream inputStream;
    private static ObjectOutputStream outputStream;
    private static Socket clinetSocket;

    public static synchronized clientConnection getConnection(){
        if(connection==null){
            connection = new clientConnection();
        }
        return connection;
    }

    public void Start() throws IOException {

        clinetSocket = new Socket("localhost", 8002);
        outputStream = new ObjectOutputStream(clinetSocket.getOutputStream());
        inputStream = new ObjectInputStream((clinetSocket.getInputStream()));

    }

    public void SendToServer(Order order) throws IOException {
        outputStream.writeObject(order);
    }


    public <T>T GetFromServer(Order order) throws IOException, ClassNotFoundException {
        SendToServer(order);
        System.out.print("getFromServer");
        return (T)inputStream.readObject();
    }
}
