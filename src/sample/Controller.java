package sample;


import java.awt.event.*;
import java.io.IOException;
import java.net.URL;
import java.security.KeyException;
import java.security.PublicKey;
import java.util.ResourceBundle;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.animation.KeyFrame;
import javafx.animation.PathTransition;
import javafx.event.*;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Polyline;
import javafx.util.Duration;

import javax.xml.crypto.dsig.keyinfo.KeyValue;

public class Controller {

    //variables
    public boolean isLogin = true;
    clientConnection connection = new clientConnection();
    String answer;
    Parent root;
    Stage primaryStage;
    FXMLLoader fxmlLoader;

    /*public Controller(Parent _root, Stage _primaryStage, FXMLLoader _fxmlLoader) throws IOException {
        root = _root;
        primaryStage = _primaryStage;
        fxmlLoader = _fxmlLoader;
    }*/

    public boolean GetInfo(Parent _root, Stage _primaryStage){
        root = _root;
        primaryStage = _primaryStage;
        //fxmlLoader = _fxmlLoader;
        return true;
    }
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private AnchorPane registrationPanel;

    @FXML
    private Label textOk;

    @FXML
    private Button buttonOK;

    @FXML
    private TextField nameFieldRegistr;

    @FXML
    private TextField surnameFieldRegistr;

    @FXML
    private TextField positionFieldRegistr;

    @FXML
    private TextField loginField;

    @FXML
    private Label failText;

    @FXML
    private Button backRegistration;

    @FXML
    private Button backLogin;

    @FXML
    private Button registration;

    @FXML
    private Button login;

    @FXML
    private TextField passwordField;
    @FXML
    void initialize() throws IOException {
        clientConnection.getConnection().Start();
        buttonOK.setOnAction(actionEvent -> {
            //System.out.print(Client.getClient());
            if(isLogin==true)
            {
                Order order = new Order();
                order.setOrder("2",loginField.getText(), passwordField.getText(),
                                " ", " ", " ");
                try {
                    answer=clientConnection.getConnection().GetFromServer(order);
                    System.out.print(answer);
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }

                if(answer.equals("2")){
                    failText.setVisible(true);
                }
                else {
                    buttonOK.getScene().getWindow().hide();

                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("projectManager.fxml"));

                    try {
                        loader.load();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Parent root = loader.getRoot();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(root));
                    stage.initStyle(StageStyle.UNDECORATED);
                    //stage.setFullScreen(true);
                    stage.show();
                }
            }
            else {
                Order order = new Order();
                order.setOrder("1",loginField.getText(), passwordField.getText(),
                        nameFieldRegistr.getText(), surnameFieldRegistr.getText(), positionFieldRegistr.getText());
                try {
                    answer=clientConnection.getConnection().GetFromServer(order);
                    System.out.print(answer);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }

        });

        registration.setOnAction(actionEvent -> {
            System.out.print("button registr");
            if(isLogin==true) {
                isLogin = false;
                login.setVisible(true);
                backRegistration.setVisible(false);
                backLogin.setVisible(true);
                failText.setVisible(false);

                Polyline polyline = new Polyline();
                polyline.getPoints().addAll(new Double[]{
                        323.0, 213.0,
                        323.0, 640.0,
                        323.0, 595.0

                });

                PathTransition transition = new PathTransition();
                transition.setNode(registrationPanel);
                transition.setDuration(Duration.seconds(1));
                transition.setPath(polyline);
                transition.play();
            }
        });

        login.setOnAction(actionEvent -> {
            System.out.print("button login");
            if(isLogin==false) {
                isLogin = true;
                backRegistration.setVisible(true);
                backLogin.setVisible(false);

                Polyline polyline = new Polyline();
                polyline.getPoints().addAll(new Double[]{
                        323.0, 595.0,
                        323.0, 213.0,
                });

                PathTransition transition = new PathTransition();
                transition.setNode(registrationPanel);
                transition.setDuration(Duration.seconds(0.7));
                transition.setPath(polyline);
                transition.play();
            }
        });

    }
    public final void setOnCloseRequest(EventHandler<WindowEvent> value){
        System.out.print("1111111");
    }



}

