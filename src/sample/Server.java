package sample;

import java.io.*;
import java.net.*;

public class Server {
    public static void main(String[] args) {
        DB_Controller.connect();
        Server server = new Server();
        server.start(8002);
    }

    private int clientCounter = 0;
    ServerSocket serverSocket;

    public void start(int port){
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Can't start the server on port " + port);
            System.out.println(e.getMessage() +" ");
            e.printStackTrace();
            System.exit(-1);
        }

        while (true) {
            Socket clientSocket;
            try {
                System.out.print("Waiting for a client...");
                clientSocket = serverSocket.accept();

                ObjectInputStream is = new ObjectInputStream(clientSocket.getInputStream());
                ObjectOutputStream os = new ObjectOutputStream(clientSocket.getOutputStream());

                System.out.println("Client #" + (++clientCounter) + " connected ("
                        + clientSocket.getInetAddress().toString() + ":" + clientSocket.getPort() + ")");

                new Thread(new ClientThread(clientSocket, clientCounter, os, is)).start();
            } catch (IOException e) {
                System.out.println("Can't accept client!");
                System.exit(-1);
            }
        }

    }
}
