package sample;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.animation.PathTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Polyline;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class ProjectManager {


    boolean isEdit = false;
    boolean isActivated = false;
    boolean itsEdit = true;
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button backProject;

    @FXML
    private Button backStaff;

    @FXML
    private Button backProfit;

    @FXML
    private Button backPreStaff;

    @FXML
    private Button backProfile;

    @FXML
    private Button profile;

    @FXML
    private Button staff;

    @FXML
    private Button project;

    @FXML
    private Button profit;

    @FXML
    private Button preStaff;

    @FXML
    private Button buttonClose;

    @FXML
    private Button buttonMIN;

    @FXML
    private AnchorPane staffPanel;

    @FXML
    private AnchorPane projectPanel;

    @FXML
    private AnchorPane profitPanel;

    @FXML
    private AnchorPane preStaffPanel;

    @FXML
    private TableView<TableUsers> table_Users = new TableView<>();

    /*@FXML
    private TableColumn<TableUsers, String> tableUsersID;

    @FXML
    private TableColumn<TableUsers, String> tableUsersName;

    @FXML
    private TableColumn<TableUsers, String> tableUsersSurname;

    @FXML
    private TableColumn<TableUs
    @FXML
    private TableColumn<TableUsers, String> tableUsersHours;
ers, String> tableUsersPosition;

    @FXML
    private TableColumn<TableUsers, String> tableUsersWork;*/

    @FXML
    private TableView<preUser> table_futereUser;

    @FXML
    private ImageView redCircle;

    @FXML
    private Label attention;
    @FXML
    private Button buttonDdelete;

    @FXML
    private Button editButton;
    @FXML
    private Button addButton;
    @FXML
    private TextField idField;

    @FXML
    private TextField nameField;

    @FXML
    private TextField surnameField;

    @FXML
    private TextField positionField;

    @FXML
    private TextField hoursField;

    @FXML
    private TextField workField;

    @FXML
    private Button editOk;

    @FXML
    private AnchorPane alertPanel;

    @FXML
    private Text alertText;

    @FXML
    private Button alertOk;
    @FXML
    private Button buttonAccept;

    @FXML
    private Button buttonIgnore;



    @FXML
    private AnchorPane editPanel;
    ObservableList<TableUsers> oblist = FXCollections.observableArrayList();
    ObservableList<preUser> oblist1 = FXCollections.observableArrayList();

    List<preUser> pre1 = null;
    List<TableUsers> tableUsers = null;

    @FXML
    void initialize() throws IOException, ClassNotFoundException {

        alertPanel.setVisible(false);


        TableColumn tableUsersID = new TableColumn("ID");
        tableUsersID.setCellValueFactory(new PropertyValueFactory<>("id"));
        TableColumn tableUsersName = new TableColumn("NAME");
        tableUsersName.setCellValueFactory(new PropertyValueFactory<>("name"));
        TableColumn tableUsersSurname = new TableColumn("SURNAME");
        tableUsersSurname.setCellValueFactory(new PropertyValueFactory<>("surname"));
        TableColumn tableUsersPosition = new TableColumn("POSITION");
        tableUsersPosition.setCellValueFactory(new PropertyValueFactory<>("position"));
        TableColumn tableUsersHours = new TableColumn("HOURS");
        tableUsersHours.setCellValueFactory(new PropertyValueFactory<>("hours"));
        TableColumn tableUsersWork = new TableColumn("WORK PERCENT");
        tableUsersWork.setCellValueFactory(new PropertyValueFactory<>("work"));

        //Button button = new Button("delete");

        //tableButtonDelete.setOnAction();


        table_Users.getColumns().addAll(tableUsersID, tableUsersName, tableUsersSurname, tableUsersPosition, tableUsersHours, tableUsersWork);


        TableColumn tableUsersID1 = new TableColumn("ID");
        tableUsersID1.setCellValueFactory(new PropertyValueFactory<>("id"));
        TableColumn tableUsersName1 = new TableColumn("NAME");
        tableUsersName1.setCellValueFactory(new PropertyValueFactory<>("name"));
        TableColumn tableUsersSurname1 = new TableColumn("SURNAME");
        tableUsersSurname1.setCellValueFactory(new PropertyValueFactory<>("surname"));

        TableColumn tableUsersPosition1 = new TableColumn("POSITION");
        tableUsersPosition1.setCellValueFactory(new PropertyValueFactory<>("position"));


        table_futereUser.getColumns().addAll(tableUsersID1, tableUsersName1, tableUsersSurname1, tableUsersPosition1);

        RefreshTables();



        project.setOnAction(actionEvent -> {
            System.out.print("project");
            preStaffPanel.setVisible(false);
            staffPanel.setVisible(false);
            table_futereUser.setVisible(false);
            buttonDdelete.setVisible(false);
            editButton.setVisible(false);

            backProject.setVisible(false);
            backProfit.setVisible(true);
            backStaff.setVisible(true);
            backPreStaff.setVisible(true);
            backProfile.setVisible(true);
        });
        profit.setOnAction(actionEvent -> {
            System.out.print("profit");
            preStaffPanel.setVisible(false);
            staffPanel.setVisible(false);
            table_futereUser.setVisible(false);
            buttonDdelete.setVisible(false);
            editButton.setVisible(false);

            backProject.setVisible(true);
            backProfit.setVisible(false);
            backStaff.setVisible(true);
            backPreStaff.setVisible(true);
            backProfile.setVisible(true);
        });
        staff.setOnAction(actionEvent -> {
            System.out.print("staff");
            preStaffPanel.setVisible(false);
            staffPanel.setVisible(true);
            table_futereUser.setVisible(false);
            buttonDdelete.setVisible(true);
            editButton.setVisible(true);

            backProject.setVisible(true);
            backProfit.setVisible(true);
            backStaff.setVisible(false);
            backPreStaff.setVisible(true);
            backProfile.setVisible(true);



        });
        preStaff.setOnAction(actionEvent -> {
            System.out.print("prestaff");

            staffPanel.setVisible(false);
            preStaffPanel.setVisible(true);
            table_futereUser.setVisible(true);
            redCircle.setVisible(false);
            attention.setVisible(false);
            buttonDdelete.setVisible(false);
            editButton.setVisible(false);

            backProject.setVisible(true);
            backProfit.setVisible(true);
            backStaff.setVisible(true);
            backPreStaff.setVisible(false);
            backProfile.setVisible(true);



        });
        profile.setOnAction(actionEvent -> {
            staffPanel.setVisible(false);
            preStaffPanel.setVisible(false);
            table_futereUser.setVisible(true);
            redCircle.setVisible(false);
            attention.setVisible(false);
            buttonDdelete.setVisible(false);
            editButton.setVisible(false);

            backProfile.setVisible(false);
            backProject.setVisible(true);
            backProfit.setVisible(true);
            backStaff.setVisible(true);
            backPreStaff.setVisible(true);
        });


        editButton.setOnAction(actionEvent -> {
            itsEdit = true;

            if(isEdit==false &&isActivated == false) {

                Polyline polyline = new Polyline();
                polyline.getPoints().addAll(new Double[]{
                        0.0, 47.0,
                        2250.0, 47.0
                });

                PathTransition transition = new PathTransition();
                transition.setNode(editPanel);
                transition.setDuration(Duration.seconds(1));
                transition.setPath(polyline);
                transition.play();
                isEdit = true;
                isActivated =true;
            }
            else if(isEdit!=false &&isActivated == true) {

                Polyline polyline = new Polyline();
                polyline.getPoints().addAll(new Double[]{
                        2250.0, 47.0,
                        0.0, 47.0
                });

                PathTransition transition = new PathTransition();
                transition.setNode(editPanel);
                transition.setDuration(Duration.seconds(1));
                transition.setPath(polyline);
                transition.play();
                isActivated =false;
                isEdit = false;
            }


        });

        editOk.setOnAction(actionEvent -> {
            if(itsEdit == true) {
                System.out.print(" edit user!!! ");
                TableUsers user1 = table_Users.getSelectionModel().getSelectedItem();
                if (user1 != null) {
                    TableUsers user = new TableUsers(idField.getText(), nameField.getText(),
                            surnameField.getText(), positionField.getText(), hoursField.getText(), workField.getText());
                for(TableUsers useer : tableUsers){
                    if(useer.id == user1.id){
                        user1 = useer;
                        break;
                    }
                }

                    Order order = new Order();
                    order.setOrder("edit user", " ", nameField.getText(),
                            surnameField.getText(), positionField.getText(), user1.id );
                    try {
                        String answer1 =clientConnection.getConnection().GetFromServer(order);
                        System.out.print(answer1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    Order order3 = new Order();
                    order3.setOrder("edit user properties", hoursField.getText(), workField.getText(),
                            user1.id, " ", " ");
                    System.out.print(" h: " + hoursField.getText()+"w: "+workField.getText()+ "id: "+ user1.id);
                    try {
                        String answer2 =clientConnection.getConnection().GetFromServer(order3);
                        System.out.print(answer2);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    table_Users.getItems().add(user);
                    idField.clear();
                    nameField.clear();
                    surnameField.clear();
                    positionField.clear();
                    hoursField.clear();
                    workField.clear();
                    table_Users.getItems().removeAll(table_Users.getSelectionModel().getSelectedItem());
                } else {
                    alertPanel.setVisible(true);
                    alertText.setText("NO ROW SELECTED");
                }
            }
            if(itsEdit == false) {

                System.out.print(" add user!!! ");

                    TableUsers user = new TableUsers(idField.getText(), nameField.getText(),
                            surnameField.getText(), positionField.getText(), hoursField.getText(), workField.getText());


                    Order order = new Order();
                    order.setOrder("add user", "1", "1", nameField.getText(),
                            surnameField.getText(), positionField.getText() );

                    try {
                        String answer1 =clientConnection.getConnection().GetFromServer(order);
                        System.out.print("TRUE ID:"+answer1);
                        user.id = answer1;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    
                    table_Users.getItems().add(user);
                    idField.clear();
                    nameField.clear();
                    surnameField.clear();
                    positionField.clear();
                    hoursField.clear();
                    workField.clear();


            }

        });
        addButton.setOnAction(actionEvent ->{
            itsEdit = false;

            if(isEdit==false &&isActivated == false) {
                System.out.print("111111");
                Polyline polyline = new Polyline();
                polyline.getPoints().addAll(new Double[]{
                        0.0, 47.0,
                        2200.0, 47.0
                });

                PathTransition transition = new PathTransition();
                transition.setNode(editPanel);
                transition.setDuration(Duration.seconds(1));
                transition.setPath(polyline);
                transition.play();
                isEdit = true;
                isActivated =true;
            }
            else if(isEdit!=false &&isActivated == true) {
                System.out.print("2222222222");
                Polyline polyline = new Polyline();
                polyline.getPoints().addAll(new Double[]{
                        2200.0, 47.0,
                        0.0, 47.0
                });

                PathTransition transition = new PathTransition();
                transition.setNode(editPanel);
                transition.setDuration(Duration.seconds(1));
                transition.setPath(polyline);
                transition.play();

            }
        });

        buttonDdelete.setOnAction(actionEvent -> {
            TableUsers user = table_Users.getSelectionModel().getSelectedItem();
            System.out.print(" ID:"+user.id+" ");
            Order order = new Order();
            order.setOrder("delete user",user.id, " ",
                    " "," ", " ");
            try {
                String answer = clientConnection.getConnection().GetFromServer(order);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            table_Users.getItems().removeAll(table_Users.getSelectionModel().getSelectedItem());

        });

        alertOk.setOnAction(actionEvent -> {
            alertPanel.setVisible(false);
        });


        buttonAccept.setOnAction(actionEvent -> {
            preUser user1 = table_futereUser.getSelectionModel().getSelectedItem();
            if (user1 != null) {
                Order order4 = new Order();
                order4.setOrder("fromPreUserToUser",user1.id, " ",
                        " "," ", " ");
                try {
                    String answer = clientConnection.getConnection().GetFromServer(order4);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }


                    Order order5 = new Order();
                    order5.setOrder("delete preuser",user1.id, " ",
                            " "," ", " ");
                    try {
                        String answer = clientConnection.getConnection().GetFromServer(order5);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    table_futereUser.getItems().removeAll(table_futereUser.getSelectionModel().getSelectedItem());


            }

            else {
                alertPanel.setVisible(true);
                alertText.setText("NO ROW SELECTED");
            }
        });

        buttonIgnore.setOnAction(actionEvent -> {
            preUser user2 = table_futereUser.getSelectionModel().getSelectedItem();
            if (user2 != null) {
            Order order5 = new Order();
            order5.setOrder("delete preuser",user2.id, " ",
                    " "," ", " ");
            try {
                String answer = clientConnection.getConnection().GetFromServer(order5);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
                table_futereUser.getItems().removeAll(table_futereUser.getSelectionModel().getSelectedItem());

            }
            else {
                alertPanel.setVisible(true);
                alertText.setText("NO ROW SELECTED");
            }
        });
    }

    public void RefreshTables() throws IOException, ClassNotFoundException {
        Order order1 = new Order();
        order1.setOrder("getUsersTable", " ", " ", " ", " ", " ");
        tableUsers = clientConnection.getConnection().GetFromServer(order1);
        System.out.print(tableUsers.size());
        oblist.addAll(tableUsers);
        table_Users.setItems(oblist);

        Order order2 = new Order();
        order2.setOrder("getPreUsersTable", " ", " ", " ", " ", " ");
        pre1 = clientConnection.getConnection().GetFromServer(order2);
        System.out.print(pre1.size());
        attention.setText(String.valueOf(pre1.size()));
        oblist1.addAll(pre1);
        table_futereUser.setItems(oblist1);
    }


}
