package sample;

import javafx.scene.control.Button;


public class TableUsers implements java.io.Serializable{
    String id;
    String name;
    String surname;
    String position;
    String hours;
    String work;

    TableUsers(String _id, String _name, String _surname, String _position, String _hours, String _work ){
        id = _id;
        name = _name;
        surname = _surname;
        position = _position;
        hours = _hours;
        work = _work;
    }
    public String getId(){
        return id;
    }
    public String getName(){
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPosition() {
        return position;
    }

    public String getHours() {
        return hours;
    }

    public String getWork() {
        return work;
    }

}

