package sample;

import java.sql.*;


class DB_Controller {
    private static final String url = "jdbc:mysql://localhost:3306/company?autoReconnect=true&useSSL=false";
    private static final String user = "Andrey";
    private static final String password = "QwertyHjkl2203";

    static Connection connection;
    static Statement statement;
    static ResultSet resultSet;

    static void connect(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    static void show(){
        final String selectQuerry = "SELECT * FROM Users";
        try {
            resultSet = statement.executeQuery(selectQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void Add(String arg1, String arg2, String arg3, String arg4, String arg5){
        final String updateQuerry = "INSERT INTO Future_user (Name, Surname, Position, Login, Password) VALUES ('" + arg1 + "', '" + arg2 + "', '" + arg3 + "', '" + arg4 + "', '" + arg5 + "')";
        try {
            statement.executeUpdate(updateQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void AddUser(String arg1, String arg2, String arg3, String arg4, String arg5){
        final String updateQuerry = "INSERT INTO Users (Name, Surname, Position, Login, Password) VALUES ('" + arg1 + "', '" + arg2 + "', '" + arg3 + "', '" + arg4 + "', '" + arg5 + "')";
        try {
            statement.executeUpdate(updateQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final String selectQuerry = "SELECT Idusers FROM Users WHERE Name='" + arg1 + "' AND Surname = '" + arg2 + "' AND Position='" + arg3 + "' AND Login= '" + arg4 + "' AND Password='" + arg5 + "'";
        try {
            resultSet = statement.executeQuery(selectQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    static void FindUser(String arg1, String arg2, String arg3, String arg4, String arg5){

        final String selectQuerry = "SELECT * FROM Users WHERE Name='" + arg1 + "' AND Surname = '" + arg2 + "' AND Position='" + arg3 + "' AND Login= '" + arg4 + "' AND Password='" + arg5 + "'";
        try {
            resultSet = statement.executeQuery(selectQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void Search(String arg1, String arg2){
        final String selectQuerry = "SELECT * FROM Users WHERE Name = '" + arg1 + "' AND Surname ='" + arg2 + "' ";
        try {
            resultSet = statement.executeQuery(selectQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void getUser(String arg1){
        final String selectQuerry = "SELECT * FROM Users WHERE Idusers = '" + arg1 + "' ";
        try {
            resultSet = statement.executeQuery(selectQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void getUsersTable(){
        System.out.print("db base1");
        final String selectQuerry = "SELECT Idusers, Name, Surname, Position, Hours, Work FROM Users u, Work f WHERE u.Idusers = f.Iduser";
        try {
            resultSet = statement.executeQuery(selectQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void getPreUsersTable(){
        System.out.print("db base2");
        final String selectQuerry = "SELECT Idfuture_user, Name, Surname, Position FROM Future_user ";
        try {
            resultSet = statement.executeQuery(selectQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void EditUsers(String arg1, String arg2, String arg3, String arg4, String arg5){
        final String updateQuerry = "UPDATE Users SET Name = '" + arg2 + "', Surname = '" + arg3 + "', Position = '" + arg4 + "' WHERE Idusers ='" + arg5 +"'" ;
        System.out.print(updateQuerry);
        try {
            statement.executeUpdate(updateQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void EditUserProperties(String arg1, String arg2, String arg3){
        final String updateQuerry = "UPDATE Work SET Hours = '" + arg1 + "', Work = '" + arg2 + "' WHERE Iduser ='" + arg3 +"'" ;
        System.out.print(updateQuerry);
        try {
            statement.executeUpdate(updateQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    static void AddUserWork(String arg1, String arg2){
        final String updateQuerry = "INSERT INTO Work (Name, Surname, Position, Login, Password) VALUES ('" + arg1 + "', '" + arg2 +  "')";
        try {
            statement.executeUpdate(updateQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void DeleteUser(String arg1){
        final String updateQuerry = "DELETE FROM Work WHERE Iduser = '" + arg1 + "'";
        try {
            statement.executeUpdate(updateQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        final String updateQuerry1 = "DELETE FROM Users WHERE Idusers = '" + arg1 + "'";
        try {
            statement.executeUpdate(updateQuerry1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    static void DeletePreUser(String arg1){

        final String updateQuerry1 = "DELETE FROM Future_user WHERE Idfuture_user = '" + arg1 + "'";
        try {
            statement.executeUpdate(updateQuerry1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void fromPreUserToUser(String arg){
        final String updateQuerry = "INSERT INTO Users (Name, Surname, Position, Login, Password)(SELECT Future_user.Name, Future_user.Surname, Future_user.Position, Future_user.Login, Future_user.Password FROM Future_user WHERE Future_user.Idfuture_user = '" +arg +"')";
        try {
            statement.executeUpdate(updateQuerry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
